#Josh Sauder

##This is the source code for my [personal website](joshsauder.com). This website was build using React via the [create-react-app](https://facebook.github.io/create-react-app/) CLI tool.

#Tech Stack
1. [ReactJS](https://reactjs.org) - Javascript Framework
2. [Bootstrap](https://getbootstrap.com) - CSS Framework
3. [Nodemailer](https://nodemailer.com/about/) - NodeJS module used to e-mail source code requests and feedback to myself.
4. [Netlify](https://www.netlify.com) - Static Site Hosting
5. [Netlify Function](https://www.netlify.com/docs/functions/) - The serverless function API used to request access/send feedback to my email. It's based on Amazon Web Services Lambda function.

#ReactJS Components
1. [React-Bootstrap](https://react-bootstrap.github.io) - Bootstrap component built for React
2. [React-Particles-js](https://www.npmjs.com/package/react-particles-js) - ParticlesJS component for React that gives the background used on my home page.
3. [React-Reveal](https://www.react-reveal.com) - Animation library used to gives elements of your webpage animations.
4. [React-Scroll](https://www.npmjs.com/package/react-scripts) - Allows for vertical animated scrolling