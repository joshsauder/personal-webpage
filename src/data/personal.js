export const ProfessionalList = [
    "My goal, as a developer, is to create applications that better the users life. I truely believe software can positively impact a persons life and help make the world a much better place!",
    "One trait that has always helped me professionally is my curiosity. I've always been a curious person and learning a new skill has never been something I've shied away from. It has always helped me to increase my knowledge set, and broadens my horizons.",
    "During my Co-op at CAS, I found that I really enjoyed web development and learning the various Javascript frameworks/backend options. I also really enjoyed working with the other developers to build web apps and find ways to make other employees jobs easier.",
    "I've recently started spending a considerable amount of my free-time learning Swift and iOS app development, and just recently released my first iOS app, In The Clear."
]

export const WhatIKnowList = [
    "Languages and Frameworks - Javascript, AngularJS, HTML5, CSS, Bootstrap, ExpressJS, MSSQL, MySQL, Java, C++, Swift, Spring",
    "IDE/Text Editor - XCode, IntelliJ, Visual Studio Code, Eclipse",
    "Version Control - Git",
    "Project Management - Jira, Trello, Mantis"
]

export const WhatImLearningList = [
    "Currently - NodeJS, ReactJS, XCTest, AWS Lambda",
    "Upcoming - MongoDB, Mocha, GraphQL, Gulp"
]

export const PersonalList = [
    "I was born in Pettisville, OH (Toledo area), and graduated from Pettisville High School with a class of 50. Pretty small right!",
    "I then went to and graduated from The Ohio State University. As you can probably imagine, there was a bit of culture shock going from a small school to a school the size of OSU.",
    "In my free-time you'll most likely find me outside running. I'm sort of a self-proclaimed running fanatic. I've run 2 full marathons, 3 half marathons, and numerous 5K and 10K races. If you're wondering what I'm doing after work, there's a good chance I'm out running.",
    "I also love traveling and exploring new places. It forces me to get out of my confort zone and exposes me to new cuisines, and experiences. It also allows me to venture around and see various places and landmarks.",
    "Finally, I'm a huge Ohio State Buckeye fan. I love watching OSU football and basketball whether I'm at the game, or watching it on TV."
]

export const VolunteerWorkList = [
    "I've always been a firm believer that volunteer work is a vital aspect of a persons life. Not only for the personal benefits, but also because it benefits the communities involved.",
    "I've always enjoyed helping those with special needs, and always loved participating in athletics. I also noticed that those with special needs werent't always given the opportunity to participate in athletics earlier in life.",
    "I recently started volunteering at Special Olympic events and found it to be an awesome experience watching these individuals do things that others told them they couldn't. The encouragement and support they are given by the fans and volunteers is a fun sight to see, and gives the athletes an environment were they can be proud of there achievements."
]