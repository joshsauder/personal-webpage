export const InTheClearList = [
    "This app allows the user to determine what weather conditions they should expect to be experiencing during their travels.",
    "It includes a personalized route line that is colored based on the weather conditions, and a table that shows the user, city-by-city, what weather and temperatures they should expect.",
    "I've always had a fascination with meterology. A common problem with living in the midwest is that when you travel, you can experience a variety of different weather conditions, some of which are not safe to be driving in. This app solves this issue by telling the user what types of weather they should expect to be experiencing during their travels.",
    "Built using Swift",
    "Podfiles used include Google Maps, Google Places, AlamoFire, and SwiftlyJSON"
]

export const PersonalWebsiteList = [
    "A website that shows what I've done both professionally and in my free time.",
    "I've always enjoyed web development and a personal web page is something that has always been on my list of things to create.",
    "It's basically a great way to find out what I've done, and an easy way to get to know me.",
    "Build using ReactJS, Bootstrap, NodeJS, and Netlify Functions."
]

export const InTheClearWebList = [
    "A web app that will give the user all the functionality of the In The Clear iOS app.",
    "It will be built using ReactJS, NodeJS, AWS Lambda, and most likely MongoDB.",
    "It will also use Google Maps and Google Places similar to the iOS App.",
    "Since this app isn't free, I will need to implement a user sign-in component (using MongoDB) on both the web app and iOS app."
]

export const InTheClearPlansList = [
    "Implement a backend API to limit the number of API calls the users device has to make.",
    "Allow the user to plan stops (overnight hotel stays) along the way.",
    "Give the user the option to input a planned start time so they can plan ahead.",
    "Tell the user if they will experience any severe weather (blizzard conditions, or severe storms) along the way."
]